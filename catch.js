//maid robot

const cleanRoom = new Promise((resolve, reject) =>{
    let haveBattery = true
    if(haveBattery){
        resolve(`Limpando quarto... quarto limpo!`)
    }else{
        reject(`Bateria acabando... Desligando...`)
    }
});

/*cleanRoom.then(
    result => console.log(result)
).catch(
    err => console.log(err)
);*/

const process = async () => { 
    try{
        const result = await cleanRoom;
        console.log(result)
    }catch(err){
        console.log(err)
    }
};

process()