
const createTable = (characters) => {
    const tbody = document.querySelector('tbody')
    for (let i = 0; i < characters.length; i++) { 
        let character = characters[i]
        if(character.name != null){
            tbody.innerHTML += `<tr>
                                    <td>${character.name}</td>
                                    <td>${character.location}</td>
                                    <td>${character.quote}</td>
                                </tr>`
        }
    }
}

const getCharacters = async () => {
    try{
        let div = document.querySelector('div')
        div.innerHTML = 'Carregando...'

        const result = await fetch('https://safe-gorge-61722.herokuapp.com/characters');

        const json = await result.json();
        console.log(json)
        createTable(json);

        div.innerHTML = ""
    }catch(err) {
        const div = document.querySelector('div')
        div.innerHTML = "Erro!"
        console.log(err)
    }

}

getCharacters();